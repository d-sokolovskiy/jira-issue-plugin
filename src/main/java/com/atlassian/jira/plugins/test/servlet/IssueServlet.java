package com.atlassian.jira.plugins.test.servlet;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class IssueServlet extends HttpServlet {
    private IssueService issueService;
    private ProjectService projectService;
    private SearchService searchService;
    private UserManager userManager;
    private TemplateRenderer renderer;
    private com.atlassian.jira.user.util.UserManager jiraUserManager;
    private static final String LIST_BROWSER_TEMPLATE = "/templates/list.vm";

    public IssueServlet(IssueService issueService, ProjectService projectService, SearchService searchService,
                     UserManager userManager, com.atlassian.jira.user.util.UserManager jiraUserManager,
                     TemplateRenderer templateRenderer) {
        this.issueService = issueService;
        this.projectService = projectService;
        this.searchService = searchService;
        this.userManager = userManager;
        this.renderer = templateRenderer;
        this.jiraUserManager = jiraUserManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Issue> issues = getIssues(req);
        Map<String, Object> context = Maps.newHashMap();
        context.put("issues", issues);
        resp.setContentType("text/html;charset=utf-8");
        renderer.render(LIST_BROWSER_TEMPLATE, context, resp.getWriter());
    }

    private List<Issue> getIssues(HttpServletRequest req) {
        ApplicationUser user = getCurrentUser(req);
        JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
        com.atlassian.query.Query query = jqlClauseBuilder.project("TEST").buildQuery();
        PagerFilter pagerFilter = PagerFilter.getUnlimitedFilter();
        com.atlassian.jira.issue.search.SearchResults searchResults = null;
        try {
            searchResults = searchService.search(user, query, pagerFilter);
        } catch (SearchException e) {
            e.printStackTrace();
        }
        return searchResults.getIssues();
    }

    private ApplicationUser getCurrentUser(HttpServletRequest req) {
        return jiraUserManager.getUser(userManager.getRemoteUsername(req));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationUser user = getCurrentUser(req);
        String respStr = "";
        String[] issueKeysToDelete = req.getParameter("keys").split(",");
        if(issueKeysToDelete.length == 0) {
            return;
        }

        List<IssueService.IssueResult> issuesToDelete = new ArrayList<>(issueKeysToDelete.length);
        for (String key : issueKeysToDelete) {
            IssueService.IssueResult issue = issueService.getIssue(user, key);
            if(!issue.isValid()) {
                respStr = "{ \"success\" : \"false\", error: \"Couldn't find issue\"}";
                break;
            }

            IssueService.DeleteValidationResult result = issueService.validateDelete(user, issue.getIssue().getId());
            if (result.getErrorCollection().hasAnyErrors()) {
                respStr = "{ \"success\": \"false\", error: \"" + result.getErrorCollection().getErrors().get(0) + "\" }";
                break;
            } else {
                issueService.delete(user, result);
                respStr = "{ \"success\" : \"true\" }";
            }
        }
        respStr = "{ \"success\" : \"true\" }";
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().write(respStr);
    }
}