function deleteAllChecked() {
    var selected = [];
    $('#issues-table input:checked').each(function () {
        selected.push($(this).attr('name'));
    });
    if (selected.length > 0) {
        jQuery.ajax({
            type: "delete",
            url: "issueservlet?keys=" + selected.join(','),
            success: function (data) {
                selected.forEach(
                    function (item) {
                        $('#issue-row-' + item).remove();
                    }
                );
            },
            error: function () {
                console.log('error', arguments);
            }
        });
    }
}